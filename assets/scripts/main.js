// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;( function( $, window, document, undefined ) {

	"use strict";

    $(".js-menu-button").on("click", function(){
        $(".js-navigation").toggleClass("drawer--visible");  
    });
    
} )( jQuery, window, document );